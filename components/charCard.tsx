import { useFavoritesStore } from "@/lib/store";
import Image from "next/image";
import Link from "next/link";
import React, { use, useState } from 'react'
import { Button } from '@/components/ui/button'

type CharCardProps = {
  id: number;
  name: string;
  status: string;
  species: string;
  image: string;
  pageNumber?: number;
  children?: React.ReactNode;
}

export default function CharCard({ id, name, status, species, image }: CharCardProps) {
  const [isImageLoaded, setIsImageLoaded] = useState(false);

  const toggleFavorite = useFavoritesStore(state => state.toggleFavorite);

  return (
    <div className="border bg-[#87F54E] rounded p-1 max-w-[1200px] m-auto">
      <div className="relative h-[210px] lg:h-[240px] md:h-[215px] w-full">
        <div className="height-[150px] min-w-[120px] sm:min-w-[100px]">
          <Image
            src={image}
            alt={name}
            width={150}
            height={150}
            // layout="fill"
            priority={true}
            style={{ objectFit: "cover" }}
            onLoad={() => setIsImageLoaded(true)}
          />
        </div>
        <div>
          <Link href={`/characters/${id}`}>
            <p className="text-foreground text-lg max-w-[150px]">{name}</p>
          </Link>
          <p className="text-white">{status} - {species}</p>
        </div>

        <div className="flex items-center space-x-2">
          <Button
            className="absolute top-2 right-2"
            variant="ghost"
            size='icon-small'
            shape="round"
            onClick={() => toggleFavorite(id)}>
            {useFavoritesStore(state => state.favorites?.includes(id) ? '❤️' : '🤍')}
          </Button>
        </div>
      </div>
    </div>
  )
}
