import React from 'react';
//useForm - хук для работы с формами, который позволяет управлять состоянием формы и ее валидацией
import { useForm, SubmitHandler } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';//zodResolver - это пакет, который позволяет использовать zod с react-hook-form
import { z } from 'zod';
import { Input } from '@/components/ui/input'
import { Button } from '@/components/ui/button'

const schema = z.object({
  firstName: z.string().nonempty('First name is required'),
  lastName: z.string().nonempty('Last name is required'),
  email: z.string().email('Invalid email address').nonempty('Email is required'),
  phone: z.number().positive('Phone number is required')
})

//infer - это ключ слово, которое позволяет извлекать тип из др типа (в данном случае из схемы), чтобы не писать типы дважды
type FormValues = z.infer<typeof schema>
//или так
// type FormValues = {
//   firstName: string
//   lastName: string
//   email: string
//   phone: number
// };

interface FormProps {
  onClose: () => void;
}

export const FormHome: React.FC<FormProps> = ({ onClose }) => {
  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting  },
    reset,
    watch
  } = useForm<FormValues>({
    resolver: zodResolver(schema),//resolver - это функция, которая принимает схему и возвращает функцию, которая принимает данные формы и возвращает объект с ошибками
  });

  const onSubmit: SubmitHandler<FormValues> = (data) => {
    onClose();
    reset();
  };

  return (
    <div className="relative top-0 left-0 sm:fixed sm:top-0 sm:left-0 w-full h-full flex flex-col justify-center items-center bg-gray-800 bg-opacity-10 z-50">
      <div className="relative bg-white p-4 rounded shadow-lg sm:p-8">
        <form
          className=" flex flex-col gap-4"
          onSubmit={handleSubmit(onSubmit)}
        >
          <button
            className='absolute top-2 right-2'
            onClick={onClose}
          >
            &#10006;
          </button>
          <label htmlFor="firstName"> First Name</label>
          {errors.firstName && (
            <p className='text-red-500 absolute right-9'>{errors.firstName.message}</p>)}
          <Input {...register("firstName")}
            placeholder='firstname'
            className='w-72 md:w-96'
            size='large'
            id="firstName"
          />
          <label htmlFor="lastName"> Last Name</label>
          {errors.lastName && (
            <p className='text-red-500 absolute top-[135px] right-9'>{errors.lastName.message}</p>)}
          <Input {...register("lastName")}
            placeholder='lastname'
            className='w-72 md:w-96'
            size='large'
            id="lastName"
          />
          <label htmlFor="email"> Email</label>
          {errors.email && (
            <p className='text-red-500 absolute top-[235px] right-9'>{errors.email.message}</p>)}
          <Input {...register("email")}
            placeholder='email'
            className='w-72 md:w-96'
            size='large'
            id="email"
          />
          <label htmlFor="phone"> Phone </label>
          {errors.phone && (
            <p className='text-red-500 absolute top-[335px] right-9'>{errors.phone.message}</p>)}
          <Input type="number" {...register("phone")}//register
            placeholder='38 (0__) ___ __ __'
            className='w-72 md:w-96'
            size='large'
            id="phone"
          />
          <Button
            type="submit"
            variant="primary"
            align='center'
            size='icon-large'
            className='w-20'
            disabled={isSubmitting}
          >
            {isSubmitting ? 'Loading...' : 'Submit'}
          </Button>
        </form>
      </div>
    </div>
  );
};

