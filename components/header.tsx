import Link from 'next/link'
import React from 'react'
import { Button } from '@/components/ui/button'

export default function Header() {
  return (
    <div className='w-full bg-transparent border-b flex items-center border-none px-10 min-w-[360px]' style={{ backgroundImage: 'url("/images/cielo-estrellado 7.jpg")', height: 'var(--header-height)' }}>
      <div className="hidden h-full w-[280px] sm:block" style={{ backgroundImage: 'url("/images/Group1.png")'}}/>
      <div className='flex ml-auto'>
        <Button size="small">
          <Link href='/' className='font-black text-lg px-1 gap-2 text-white sm:text-xl sm:px-3 sm:gap-4'>
            Home
          </Link>
        </Button>
        <Button size="small">
          <Link href='/characters' className='font-black text-lg px-1 gap-2 text-white sm:text-xl sm:px-3 sm:gap-4'>
            Characters
          </Link>
        </Button>
        <Button size="small">
          <Link href='/favorites' className='font-black text-lg px-1 gap-2 text-white sm:text-xl sm:px-3 sm:gap-4'>
            Favorites
          </Link>
        </Button>
      </div>
    </div>
  )
}
