import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import { FiChevronLeft, FiChevronRight } from "react-icons/fi";
import { Button } from '@/components/ui/button'

type FavoritesProps = {
  characters: Character[] | undefined;
};

export default function Favorites({ characters }: FavoritesProps) {
  const [currentIndex, setCurrentIndex] = useState(0);
  const [displayedCharacters, setDisplayedCharacters] = useState<Character[]>([]);
  const cardsPerPage = 3;

  useEffect(() => {
    if (characters && characters.length >= cardsPerPage) {
      setDisplayedCharacters(characters.slice(currentIndex, currentIndex + cardsPerPage));
    } else {
      setDisplayedCharacters(characters || []);
    }
  }, [characters, currentIndex]);

  const handleNextClick = () => {
    const lastIndex = characters ? characters.length - 1 : 0;
    if (lastIndex - currentIndex >= cardsPerPage) {//если последний индекс - текущий индекс >= кол-во карточек на странице, то мы можем перейти на след страницу и увеличить индекс на кол-во карточек на странице (т.е. на 3)
      setCurrentIndex(currentIndex + 1);
    }
  };

  const handlePrevClick = () => {
    if (currentIndex > 0) {
      setCurrentIndex(currentIndex - 1);
    }
  };

  return (
    <div className="flex flex-col items-center">
      <div className="p-10">
        <Image
          src="/images/favorites.png"
          alt="Favorites"
          width={300}
          height={300}
        />
      </div>
      <div className=" p-4 relative w-[341px] md:w-[650px] im:w-[550px] sm:w-[498px]">
        <div className="flex justify-center space-x-4 md:space-x-10 sm:space-x-5">
          {displayedCharacters.map((char) => (
            <div key={char.id} className="flex-shrink-0 w-24 md:w-36 im:w-32 sm:w-28 h-[160px] md:h-[240px] im:h-[210px] sm:h-[180px] border bg-[#87F54E] rounded p-1">
              <Image
                src={char.image}
                alt={char.name}
                width={150}
                height={150}
                priority={true}
                style={{ objectFit: "contain" }}
              />
              <Link href={`/characters/${char.id}`}>
                <h2 className="text-center hover:underline cursor-pointer text-foreground text-sm im:text-md">{char.name}</h2>
              </Link>
              <p className="text-center text-white text-sm im:text-md">{char.status} - {char.species}</p>
            </div>
          ))}
        </div>
        {characters && characters.length > cardsPerPage && (
          <div className="absolute top-1/4 transform -translate-y-50 flex space-x-[230px] md:space-x-[540px] im:space-x-[440px] sm:space-x-[390px]">
            <Button
              onClick={handlePrevClick}
              className={`p-2 bg-gray-300 hover:bg-gray-400 rounded-full focus:outline-none ${currentIndex > 0 ? "" : "pointer-events-none opacity-50"
                }`}
            >
              <FiChevronLeft size={24} />
            </Button>
            <Button
              onClick={handleNextClick}
              className={`p-2 bg-gray-300 hover:bg-gray-400 rounded-full focus:outline-none ${currentIndex + cardsPerPage < (characters ? characters.length : 0)
                ? ""
                : "pointer-events-none opacity-50"
                }`}
            >
              <FiChevronRight size={24} />
            </Button>
          </div>
        )}
      </div>
    </div>
  )
}

