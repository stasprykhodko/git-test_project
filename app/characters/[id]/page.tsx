"use client"

import { useQuery } from '@tanstack/react-query'
import Image from 'next/image';
import React from 'react';

async function getCharId(id: number): Promise<Character> {
  try {
    const response = await fetch(`https://rickandmortyapi.com/api/character/${id}`);
    const data = await response.json();
    return data as Character;
  } catch (error: unknown) {
    if (error instanceof Error) {
      throw new Error(`Error fetching characters: ${error.message}`);
    }
    throw new Error(`Unknown error fetching characters`);
  }
};

export default function CharId({ params }: { params: { id: number } }) {

  const { data: char, isLoading, isError } = useQuery({
    queryKey: ['character', params.id],
    queryFn: () => getCharId(params.id),
  });

  if (isLoading) return <p>Loading...</p>
  if (isError) return <p>Error is occured...</p>

  return (
    <div className='flex justify-center pt-10 bg-cover h-[89.9vh] min-w-[360px]' style={{ backgroundImage: 'url("/images/cielo-estrellado 9.jpg")' }}>
      <div className='flex flex-col sm:flex sm:flex-row h-[450px] sm:h-[316px] border-8 border-solid bg-[#87F54E] rounded p-1'>
        <div>
          <Image
            src={char?.image}
            alt={char?.name}
            width={300}
            height={300}
            priority={true}
            style={{ objectFit: "contain" }}
          />
        </div>
        <div className='mx-1 im:mx-10'>
          <h1 className='text-foreground text-xl'>Name: {char?.name}</h1>
          <p className='text-foreground mt-2 text-xl'>Status: {char?.status}</p>
          <p className='text-foreground mt-2 text-xl'>Species: {char?.species}</p>
        </div>
      </div>
    </div>
  )
};


