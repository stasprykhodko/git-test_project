'use client'//use client это комментарий, который говорит о том, что этот файл будет использоваться только на клиенте и не будет собран в серверный бандл.
import { useQuery } from '@tanstack/react-query'
import React, { useState, useEffect } from 'react'
import axios from 'axios'
import CharCard from '@/components/charCard';
// import Favorites from '@/components/favorites';
import Link from 'next/link';
import { useFavoritesStore, usePageStore } from '@/lib/store';

type APIResponse = {
  info: Info;
  results: Character[];
}

//синтаксис Promise<APIResponse> означает, что функция fetchCharacters возвращает промис, который разрешается в объект APIResponse.
async function fetchCharacters(page: number): Promise<APIResponse> {
  try {
    // const response = await fetch(`https://rickandmortyapi.com/api/character?page=${page}`)
    const response = await fetch(`https://rickandmortyapi.com/api/character/?page=${page}`)
    const data = await response.json()
    return data as APIResponse//as APIResponse - явное привед типа, кот говорит TS, что мы уверены, что data имеет тип APIResponse. Мы указ явное привед типа, потому что TS не может опр тип данных, возвр API.
  } catch (error: unknown) {
    if (error instanceof Error) {
      throw new Error(`Error fetching characters: ${error.message}`)
    }
    throw new Error(`Unknown error fetching characters`)
  }
}

export default function Page({ searchParams }: { searchParams: { page: number } }) {
  const initFavorites = useFavoritesStore(state => state.init);
  const currentPage = usePageStore(state => state.currentPage);
  const setCurrentPage = usePageStore(state => state.setCurrentPage);

  useEffect(() => {
    initFavorites();
  }, [])

  useEffect(() => {
    setCurrentPage(searchParams.page || 1);
  }, [searchParams.page]);

  // const [currentPage, setCurrentPage] = useState(searchParams.page || 1);
  const { data: characters, isLoading, isError } = useQuery({
    queryKey: ['characters', currentPage],
    queryFn: () => fetchCharacters(currentPage),
  });
  const totalPages = characters?.info.pages ?? 0;
  const pagesToShow: number = 6;

  function generatePageNumbers(): (number | string)[] {
    const pageNumbers: (number | string)[] = [];
    const halfPagesToShow = Math.floor(pagesToShow / 2);
    let startPage = Math.max(1, currentPage - halfPagesToShow);
    let endPage = Math.min(startPage + pagesToShow - 1, totalPages);

    while (endPage - startPage + 1 < pagesToShow && startPage > 1) {
      startPage--;
    }

    while (endPage - startPage + 1 < pagesToShow && endPage < totalPages) {
      endPage++;
    }
    if (startPage > 1) {
      pageNumbers.push(1);
      if (startPage > 2) {
        pageNumbers.push('...');
      }
    }
    for (let i = startPage; i <= endPage; i++) {
      pageNumbers.push(i);
    }
    if (endPage < totalPages) {
      if (endPage < totalPages - 1) {
        pageNumbers.push('...');
      }
      pageNumbers.push(totalPages);
    }

    return pageNumbers;
  }

  // useEffect(() => {
  //   setCurrentPage(searchParams.page || 1);
  // }, [searchParams.page]);

  if (isLoading) return <p>Loading...</p>;
  if (isError) return <p>Error occurred...</p>;

  const pageNumbers = generatePageNumbers();

  return (
    <div className='bg-cover flex flex-col items-center justify-center min-w-[360px]' style={{ backgroundImage: 'url("/images/cielo-estrellado 9.jpg")' }} >
      <div className='grid grid-cols-2 lg:grid-cols-5 md:grid-cols-4 sm:grid-cols-3 gap-10 px-5 lg:px-20 md:px-15 sm:px-10 py-20 w-10/12'>
        {characters?.results.map(char => (
          <CharCard
            key={char.id}
            id={char.id}
            name={char.name}
            status={char.status}
            species={char.species}
            image={char.image}
          />
        ))}
      </div>
      <div className="w-80 h-12 pl-20 pb-20">
          {pageNumbers.map((pageNumber, index) => (
            <React.Fragment key={index}>
              {pageNumber === '...' ? (
                <span className="ellipsis text-white ">{pageNumber}</span>
              ) : (
                <Link href={`/characters?page=${pageNumber}`}>
                  <span className={`text-white px-1 text-xl`}>{pageNumber}</span>
                </Link>
              )}
            </React.Fragment>
          ))}
        </div>
    </div>
  );
}



