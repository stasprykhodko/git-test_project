'use client'

import { useQuery } from '@tanstack/react-query'
import React, { useEffect } from 'react'
import Favorites from '@/components/favorites';
import { useFavoritesStore } from '@/lib/store';

type APIResponse = {
  info: Info;
  results: Character[];
}

async function fetchFavCharacters(): Promise<Character[]> {
  try {
    const allCharacters: Character[] = [];

    const response = await fetch(`https://rickandmortyapi.com/api/character`);
    const data: APIResponse = await response.json();
    const totalPages = data?.info?.pages ?? 0;

    for (let page = 1; page <= totalPages; page++) {
      const response = await fetch(`https://rickandmortyapi.com/api/character/?page=${page}`);
      const data: APIResponse = await response.json();

      if (data.results && data.results.length > 0) {
        allCharacters.push(...data.results);
      } else {
        break; 
      }
    }
    return allCharacters;
  } catch (error: unknown) {
    if (error instanceof Error) {
      throw new Error(`Error fetching characters: ${error.message}`);
    }
    throw new Error(`Unknown error fetching characters`);
  }
}

export default function FavPage() {
  const { data: characters, isLoading, isError } = useQuery({
    queryKey: ['characters'],
    queryFn: fetchFavCharacters, // Используем новую функцию для загрузки персонажей.
  });

  const initFavorites = useFavoritesStore(state => state.init);
  const favorites = useFavoritesStore(state => state.favorites);
  const filteredCharacters = characters?.filter(char => favorites.includes(char.id)) ?? [];

  useEffect(() => {
    initFavorites();
  }, [])

  if (isLoading) return <p>Loading...</p>
  if (isError || !characters || characters.length === 0) {
    return <p>Error occurred or no characters found.</p>;
  }

  return (
    <div className='bg-cover h-[89.9vh] min-w-[360px]' style={{ backgroundImage: 'url("/images/cielo-estrellado 9.jpg")' }}>
      {/* <div className='text-xl text-white'>hello</div> */}
      {characters && Array.isArray(characters) ? (
        <Favorites characters={filteredCharacters} />
      ) : (
        <p>No characters found.</p>
      )}
    </div>
  )
}

