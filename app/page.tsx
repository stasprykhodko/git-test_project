"use client"

import React, { useState, useEffect } from 'react';
import { FormHome } from '@/components/formHome';
import { Button } from '@/components/ui/button'

export default function Home() {
  const [isFormOpen, setIsFormOpen] = useState(false);
  const [bgImageUrl, setBgImageUrl] = useState('/images/Group_4.png');

  const openForm = () => {
    setIsFormOpen(true);
  };

  const closeForm = () => {
    setIsFormOpen(false);
  };

  useEffect(() => {
    const handleResize = () => {
      const newImageUrl = window.innerWidth >= 501 ? '/images/Group_8.png' : '/images/Group_4.png';
      setBgImageUrl(newImageUrl);
    };
    window.addEventListener('resize', handleResize);
    handleResize();

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div className="relative flex flex-col items-center justify-center h-[89.9vh] bg-gray-500 bg-cover bg-center max-h-full min-w-[360px]"
      style={{ backgroundImage: 'url("/images/fondo-hiperespacial-3d-efecto-tunel-urdimbre 1.jpg")' }}
    >
      <div className='absolute top-10 left-10'>
        <Button onClick={openForm}>Open Form</Button>
        {isFormOpen && <FormHome onClose={closeForm} />}
      </div>
      <div className="flex items-center justify-center">
        <div
          className="bg-content bg-no-repeat h-[175px] w-[320px] sm:h-[261px] sm:w-[479px]"
          style={{ backgroundImage: `url(${bgImageUrl})` }}
        />
      </div>
    </div>
  );
}