interface Character {
  id: number;
  name: string;
  status: string;
  species: string;
  type: string;
  gender: string;
  origin: CharOrigin;
  location: CharLocation;
  image: string;
  episode: string[];
  url: string;
  created: string;
}

interface CharOrigin {
  name: string;
  url: string;
}

interface CharLocation {
  name: string;
  url: string;
}