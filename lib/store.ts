import { create } from 'zustand';

type FavoritesStore = {
  favorites: number[];
  init: () => void;
  toggleFavorite: (id: number) => void;
};

type PageStore = {
  currentPage: number;
  setCurrentPage: (page: number) => void;
}

export const useFavoritesStore = create<FavoritesStore>((set, get) => ({
  favorites: [],

  init: async () => {
    if (typeof window !== "undefined") {
      const storedFavorites = localStorage.getItem('favorites');
      //initialState д.б. объектом с ключом favorites, кот явл [] чисел.
      const initialState: { favorites: number[] } = storedFavorites ? JSON.parse(storedFavorites) : { favorites: [] };
      set({ favorites: initialState.favorites });
    }
  },

  toggleFavorite: (id: number) => {
    if (typeof window !== "undefined") {
      //"storedFavorites: {"favorites":[2,821,1]}" пока в виде строки
      const storedFavorites = localStorage.getItem('favorites');
      //initialState: {"favorites":[2,821,1]} уже в виде объекта
      const initialState: { favorites: number[] } = storedFavorites ? JSON.parse(storedFavorites) : { favorites: [] };

      const newFavorites = initialState.favorites.includes(id)
        ? initialState.favorites.filter((favId) => favId !== id)
        : [...initialState.favorites, id];

      localStorage.setItem('favorites', JSON.stringify({ favorites: newFavorites }));

      set({
        favorites: newFavorites,
      });
    }
  },
}));

export const usePageStore = create<PageStore>((set, get) => ({
  currentPage: 1,
  setCurrentPage: (page) => set({ currentPage: page }),
}));





